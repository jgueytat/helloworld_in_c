[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://bitbucket.org/jgueytat/helloworld_in_c/src/master/)

# HelloWorld in C / C++ with VSCode and Gitpod

## VSCode / Gitpod specific files

### Specific files for VSCode

* **.vscode/** : 
    * **launch.json** : set of commands for debugging
* **.devcontainer/** :
    * **devcontainer.json** : docker environment image (cmake, gcc, gdb,...) + installed extensions

### Specific files for Gitpod

* **.theia/** :
    * **launch.json** : : set of commands for debugging
* **.gitpod.Dockerfile** : docker environment image (cmake, gcc, gdb,...)
* **.gitpod.yml** : installed extensions
